# VMs in the Venus Node

## VCENTER - 192.168.100.80
8GB 2 cores 500gb

## Gitlab CICD: CI - 192.168.100.157
RedHat 9.3
6 GB 2 cores 100GB

build server, code repo, artifact repo, scanning tools, test tools, web, app, db

## Vault - 192.168.100.138
Debian 12
4 GB 2 cores

## Docker - 192.168.100.138
Debian 12
6 GB 4 cores

## Harbor: Image Registry

You can integrate a different CI, deploy locally (different VM) or plug into docker/GKE/etc 

## System - 192.168.100.141
DNS and other system services 


### Ansible Terraform Container
Dockerfile using your based favorite Linux distro, have the dockerfile install python3+pip, then install ansible via pip. Then use your image for executing playbooks and so on. Mount your ansible directory into it so you still have all the playbooks and configs locally, and create an alias for executing the commands against the Docker container. This way you could almost feel like it was running locally, but it all happens inside the container.

alias terraform='docker run --rm -it -w $PWD:$PWD -v $PWD:$PWD terraform:whateverversion'

## Kubernetes RedHat - Containerd
12 GB (4 cores and 2G per worker)

## Podman Redhat - 

## ArgoCD to manage applications using GitOps practices
